CURDIR=$(shell pwd)
LOCLDIR=/usr/local/bin

install: check_fileset_inodes

update: purge_links check_fileset_inodes

check_fileset_inodes:   .FORCE
	ln -s $(CURDIR)/check_fileset_inodes $(LOCLDIR)/check_fileset_inodes

clean:
	rm -f $(LOCLDIR)/check_fileset_inodes

purge_links:
	rm -f ${LOCLDIR}/check_fileset_inodes

.FORCE:


